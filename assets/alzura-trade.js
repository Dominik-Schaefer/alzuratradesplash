let mouseIsOver = false;

$(document).ready(function () {
  $("#switchcheck")
    .mouseenter(function () {
      mouseIsOver = true;
    })
    .mouseleave(function () {
      mouseIsOver = false;
    });
  doSwitch();

  /* Switching off animantion while hovering */
  $("[data-anim]").each(function () {
    $(this).on("mouseenter", function () {
      $("[data-animation]").each(function () {
        $(this).css("animation", "none");
        if ($(this).hasClass("connectiontest__circle")) {
          $(this).css("display", "none");
        }
      });
    });
  });

  $("[data-anim]").on("mouseleave", function () {
    $("[data-animation]").each(function () {
      $("[data-animation]").css("animation", "");
      if ($(this).hasClass("connectiontest__circle")) {
        $(this).css("display", "");
      }
    });
  });
});

function doSwitch() {
  setTimeout(() => {
    if (!mouseIsOver) {
      document.getElementById("switch").checked = true;
    }
  }, 3000);
}
